<?php
/**
 * Read up on the WP Template Hierarchy for
 * when this file is used
 *
 */
?>
<?php get_header(); ?>

	<nav class="page--header">
		<div class="wrapper">
			<h1><?php the_title(); ?></h1>
			<?php MOZ_Crumbs::crumbs(); ?>
		</div>
	</nav>

	<?php while(have_posts()): the_post(); ?>
		<main><?php echo wpautop(do_shortcode(get_the_content())); ?></main>
	<?php endwhile; ?>

	<?php ob_start(); ?>
	[aep_parallax id="cta" image="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/students.jpg"]
		<div class="wrapper">
			<h2>Get Started Today!</h2>
			<a href="" class="button">Create Your Profile</a>
		</div>
	[/aep_parallax]
	<?php echo do_shortcode(ob_get_clean()); ?>


<?php get_footer(); ?>
